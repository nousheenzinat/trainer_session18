<?php

namespace App;


class Utility
{

    public static function d($var){
        echo "<pre>";
        var_dump($var);
        echo "</pre>";

    }

    public static function dd($var){
        echo "<pre>";
        var_dump($var);
        echo "</pre>";
        die;
    }

    public static function redirect($url){
        header("Location: $url");
    }

}